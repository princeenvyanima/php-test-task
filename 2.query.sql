SELECT autors.last_name as 'Фамилия', autors.first_name as 'Имя', autors.middle_name as 'Отчество'
FROM autors, 
        (SELECT autor_id
         FROM autors_books 
         GROUP BY autor_id 
         HAVING COUNT(autor_id) <= 6) a 
WHERE autors.id = a.autor_id;