CREATE TABLE autors (
    id int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    last_name varchar(255) NOT NULL,
    first_name varchar(255) NOT NULL,
    middle_name varchar(255),
    address varchar(255),
    city varchar(255)
);

CREATE TABLE books (
    id int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title varchar(255) NOT NULL,
    publishing_house varchar(255),
    number_of_pages int,
    publishing_year varchar(4)
);

CREATE TABLE autors_books (
  	autor_id int(10) NOT NULL,
    book_id int(10) NOT NULL,
    FOREIGN KEY (autor_id) 
      REFERENCES autors(id) 
      ON DELETE CASCADE 
      ON UPDATE CASCADE,
    FOREIGN KEY (book_id) 
      REFERENCES books(id) 
      ON DELETE CASCADE 
      ON UPDATE CASCADE 
);