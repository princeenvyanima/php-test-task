<?php
function CamelCase($str, $separators){
    //делим строки на первое слово и оставшуюся часть
    foreach ($separators as $sep){
        $str_arr = explode($sep, $str, 2);
        if(count($str_arr) === 2){
            //запоминаем разделитель
           $current_separator = $sep;
           break; 
        }
    }
    //преобразование
    $a = str_replace($current_separator, '', ucwords($str_arr[1], $current_separator));
    return $str_arr[0] . $a;
}
$string1 = "get-books-count";
$string2 = "Set_Currency_Value";
$separators = ["_","-"];

echo CamelCase($string1, $separators),"\n";
echo CamelCase($string2, $separators);