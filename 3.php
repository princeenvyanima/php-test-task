<?php
//прямоугольник
class Rectangle
{

    private $width;
    private $height;

    public function __construct(float $width = 10, float $height = 10)
    {
        $this->width = $width;
        $this->height = $height;
    }

    public function getArea(): float
    {
        return $this->width * $this->height;
    }

    public function random(): void
    {
        $this->width = rand(1, 100);
        $this->height = rand(1, 100);
    }
}
//круг
class Circle
{

    private $radius;

    public function __construct(float $radius = 10)
    {
        $this->radius = $radius;
    }

    public function getArea(): float
    {
        return pi() * 2 * pow($this->radius, 2);
    }

    public function random(): void
    {
        $this->radius = rand(1, 100);
    }
}
//треугольник
class Triangle
{ 

    private $a; 		//сторона а
    private $b; 		//сторона b
    private $c; 		//сторона с
    private $angle_ab;	//угол между а и b
	
	//функция вычисления стороны с
    private function getC(float $a, float $b, float $angle_ab): float
    {
        return sqrt(pow($a, 2) + pow($b, 2) - 2 * $a * $b * cos(deg2rad($angle_ab)));
    }

    public function __construct(float $a = 10, float $b = 10, float $angle_ab = 90)
    {
        $this->a = $a;
        $this->b = $b;
        $this->angle_ab = $angle_ab;
        $this->c = $this->getC($this->a, $this->b, $this->angle_ab);
    }

    public function getArea(): float
    {
        return $this->a * $this->b * 1 / 2 * sin(deg2rad($this->angle_ab));
    }

    public function random(): void
    {
        $this->a = rand(1, 100);
        $this->b = rand(1, 100);
        $this->angle_ab = rand(1, 179);
        $this->c = $this->getC($this->a, $this->b, $this->angle_ab);
    }
}

//функция получения экземпляра рандомного класса и полей
function getRandFigure()
{
    $array_figures = ["Circle", "Rectangle", "Triangle"];
    $class_name = $array_figures[array_rand($array_figures, 1)];
    $figure = new $class_name();
    $figure->random();
    return $figure;
}

//функция получения и сортировки массива рандомных фигур по убыванию площади
function getArrSortRandFigures(int $count): array
{
	for ($i = 0; $i < $count; $i++) $array_figures[] = getRandFigure();
	
	//сортировка массива
	usort($array_figures, function ($a, $b)
		{
			if ($a->getArea() == $b->getArea())
			{
				return 0;
			}
			return ($a->getArea() > $b->getArea()) ? -1 : 1;
		});
	return $array_figures;
}

//функция сериализации коллекции фигур и записи в файл
function serializeAndPutInFile($collection): void
{
	$str = serialize($collection);
	file_put_contents('file.txt', $str);
}

//функция получения объектов из файла и ансериализации
function unserializeAndGetFromFile()
{
	try {
		$str = file_get_contents('file.txt');
	} catch (Exception $e) {
		echo 'Ошибка файла: ',  $e->getMessage(), "\n";
	}
	return unserialize($str);
}

$figures[] = new Circle(5);
$figures[] = new Rectangle();
$figures[] = new Triangle(5,6.7,76);
$figure = getRandFigure(); //рандомный тип и поля
echo $figure->getArea();
$figures[] = $figure;
$figure->random(); //рандомные поля
$figures[] = $figure;
$figures[] = getArrSortRandFigures(10);

serializeAndPutInFile($figures);
print_r(unserializeAndGetFromFile());

